// GPIO Module Registers difinition
#define AM335x_GPIO0_BASE      ((uint32_t *)0x44E07000)
#define AM335x_GPIO1_BASE      ((uint32_t *)0x4804C000)
#define AM335x_GPIO2_BASE      ((uint32_t *)0x481AC000)
#define AM335x_GPIO3_BASE      ((uint32_t *)0x481AE000)
#define AM335x_GPIO_LEN        ((size_t)0x1000)

#define GPIO_CTRL_OFF       (0x130/(sizeof(uint32_t *)))
#define GPIO_OE_OFF         (0x134/(sizeof(uint32_t *)))
#define GPIO_DATAIN_OFF     (0x138/(sizeof(uint32_t *)))
#define GPIO_DATAOUT_OFF    (0x13C/(sizeof(uint32_t *)))
#define GPIO_CLEAR_OFF      (0x190/(sizeof(uint32_t *)))
#define GPIO_SET_OFF        (0x194/(sizeof(uint32_t *)))

// CM_PER Register difinition
#define AM335x_CM_PER_BASE     ((uint32_t *)0x44E00000)
#define AM335x_CM_PER_LEN      ((size_t)0x400)
#define GPIO1_CLKCTRL_OFF      (0xAC/(sizeof(uint32_t *)))
#define GPIO2_CLKCTRL_OFF      (0xB0/(sizeof(uint32_t *)))
#define GPIO3_CLKCTRL_OFF      (0xB4/(sizeof(uint32_t *)))

#define GPIO_CLKCTRL_MODULE_EN (0x02) 

// Control Module Registers difinition
#define AM335x_CM_BASE         ((uint32_t *)0x44e10000)
#define AM335x_CM_LEN          ((size_t)0x2000)

#define PINMUX_SLEW_CTL    (1<<6)
#define PINMUX_RXACTIVE    (1<<5)
#define PINMUX_PUTYPE      (1<<4)
#define PINMUX_PUDEN       (1<<3)
#define PINMUX_MODE        (3)

//      Control Module Registers          Offset
#define AM335x_NC                        (0xffff)
#define AM335x_CONTROL_REVISION          (0x000/sizeof(uint32_t *))
#define AM335x_CONTROL_HWINFO            (0x004/sizeof(uint32_t *))
#define AM335x_CONTROL_SYSCONFIG         (0x010/sizeof(uint32_t *))
#define AM335x_CONTROL_STATUS            (0x040/sizeof(uint32_t *))
#define AM335x_CONTROL_EMIF_SDRAM_CONFIG (0x110/sizeof(uint32_t *))
#define AM335x_CORE_SLDO_CTRL            (0x428/sizeof(uint32_t *))
#define AM335x_MPU_SLDO_CTRL             (0x42C/sizeof(uint32_t *))
#define AM335x_CLK32KDIVRATIO_CTRL       (0x444/sizeof(uint32_t *))
#define AM335x_BANDGAP_CTRL              (0x448/sizeof(uint32_t *))
#define AM335x_BANDGAP_TRIM              (0x44C/sizeof(uint32_t *))
#define AM335x_PLL_CLKINPULOW_CTRL       (0x458/sizeof(uint32_t *))
#define AM335x_MOSC_CTRL                 (0x468/sizeof(uint32_t *))
#define AM335x_DEEPSLEEP_CTRL            (0x470/sizeof(uint32_t *))
#define AM335x_DPLL_PWR_SW_STATUS        (0x50C/sizeof(uint32_t *))
#define AM335x_DEVICE_ID                 (0x600/sizeof(uint32_t *))
#define AM335x_DEV_FEATURE               (0x604/sizeof(uint32_t *))
#define AM335x_INIT_PRIORITY_0           (0x608/sizeof(uint32_t *))
#define AM335x_INIT_PRIORITY_1           (0x60C/sizeof(uint32_t *))
#define AM335x_TPTC_CFG                  (0x614/sizeof(uint32_t *))
#define AM335x_USB_CTRL0                 (0x620/sizeof(uint32_t *))
#define AM335x_USB_STS0                  (0x624/sizeof(uint32_t *))
#define AM335x_USB_CTRL1                 (0x628/sizeof(uint32_t *))
#define AM335x_USB_STS1                  (0x62C/sizeof(uint32_t *))
#define AM335x_MAC_ID0_LO                (0x630/sizeof(uint32_t *))
#define AM335x_MAC_ID0_HI                (0x634/sizeof(uint32_t *))
#define AM335x_MAC_ID1_LO                (0x638/sizeof(uint32_t *))
#define AM335x_MAC_ID1_HI                (0x63C/sizeof(uint32_t *))
#define AM335x_DCAN_RAMINIT              (0x644/sizeof(uint32_t *))
#define AM335x_USB_WKUP_CTRL             (0x648/sizeof(uint32_t *))
#define AM335x_GMII_SEL                  (0x650/sizeof(uint32_t *))
#define AM335x_PWMSS_CTRL                (0x664/sizeof(uint32_t *))
#define AM335x_MREQPRIO_0                (0x670/sizeof(uint32_t *))
#define AM335x_MREQPRIO_1                (0x674/sizeof(uint32_t *))
#define AM335x_HW_EVENT_SEL_GRP1         (0x690/sizeof(uint32_t *))
#define AM335x_HW_EVENT_SEL_GRP2         (0x694/sizeof(uint32_t *))
#define AM335x_HW_EVENT_SEL_GRP3         (0x698/sizeof(uint32_t *))
#define AM335x_HW_EVENT_SEL_GRP4         (0x69C/sizeof(uint32_t *))
#define AM335x_SMRT_CTRL                 (0x6A0/sizeof(uint32_t *))
#define AM335x_MPUSS_HW_DEBUG_SEL        (0x6A4/sizeof(uint32_t *))
#define AM335x_MPUSS_HW_DBG_INFO         (0x6A8/sizeof(uint32_t *))
#define AM335x_VDD_MPU_OPP_050           (0x770/sizeof(uint32_t *))
#define AM335x_VDD_MPU_OPP_100           (0x774/sizeof(uint32_t *))
#define AM335x_VDD_MPU_OPP_120           (0x778/sizeof(uint32_t *))
#define AM335x_VDD_MPU_OPP_TURBO         (0x77C/sizeof(uint32_t *))
#define AM335x_VDD_CORE_OPP_050          (0x7B8/sizeof(uint32_t *))
#define AM335x_VDD_CORE_OPP_100          (0x7BC/sizeof(uint32_t *))
#define AM335x_BB_SCALE                  (0x7D0/sizeof(uint32_t *))
#define AM335x_USB_VID_PID               (0x7F4/sizeof(uint32_t *))
#define AM335x_EFUSE_SMA                 (0x7FC/sizeof(uint32_t *))
#define AM335x_CONF_GPMC_AD0             (0x800/sizeof(uint32_t *))
#define AM335x_CONF_GPMC_AD1             (0x804/sizeof(uint32_t *))
#define AM335x_CONF_GPMC_AD2             (0x808/sizeof(uint32_t *))
#define AM335x_CONF_GPMC_AD3             (0x80C/sizeof(uint32_t *))
#define AM335x_CONF_GPMC_AD4             (0x810/sizeof(uint32_t *))
#define AM335x_CONF_GPMC_AD5             (0x814/sizeof(uint32_t *))
#define AM335x_CONF_GPMC_AD6             (0x818/sizeof(uint32_t *))
#define AM335x_CONF_GPMC_AD7             (0x81C/sizeof(uint32_t *))
#define AM335x_CONF_GPMC_AD8             (0x820/sizeof(uint32_t *))
#define AM335x_CONF_GPMC_AD9             (0x824/sizeof(uint32_t *))
#define AM335x_CONF_GPMC_AD10            (0x828/sizeof(uint32_t *))
#define AM335x_CONF_GPMC_AD11            (0x82C/sizeof(uint32_t *))
#define AM335x_CONF_GPMC_AD12            (0x830/sizeof(uint32_t *))
#define AM335x_CONF_GPMC_AD13            (0x834/sizeof(uint32_t *))
#define AM335x_CONF_GPMC_AD14            (0x838/sizeof(uint32_t *))
#define AM335x_CONF_GPMC_AD15            (0x83C/sizeof(uint32_t *))
#define AM335x_CONF_GPMC_A0              (0x840/sizeof(uint32_t *))
#define AM335x_CONF_GPMC_A1              (0x844/sizeof(uint32_t *))
#define AM335x_CONF_GPMC_A2              (0x848/sizeof(uint32_t *))
#define AM335x_CONF_GPMC_A3              (0x84C/sizeof(uint32_t *))
#define AM335x_CONF_GPMC_A4              (0x850/sizeof(uint32_t *))
#define AM335x_CONF_GPMC_A5              (0x854/sizeof(uint32_t *))
#define AM335x_CONF_GPMC_A6              (0x858/sizeof(uint32_t *))
#define AM335x_CONF_GPMC_A7              (0x85C/sizeof(uint32_t *))
#define AM335x_CONF_GPMC_A8              (0x860/sizeof(uint32_t *))
#define AM335x_CONF_GPMC_A9              (0x864/sizeof(uint32_t *))
#define AM335x_CONF_GPMC_A10             (0x868/sizeof(uint32_t *))
#define AM335x_CONF_GPMC_A11             (0x86C/sizeof(uint32_t *))
#define AM335x_CONF_GPMC_WAIT0           (0x870/sizeof(uint32_t *))
#define AM335x_CONF_GPMC_WPN             (0x874/sizeof(uint32_t *))
#define AM335x_CONF_GPMC_BEN1            (0x878/sizeof(uint32_t *))
#define AM335x_CONF_GPMC_CSN0            (0x87C/sizeof(uint32_t *))
#define AM335x_CONF_GPMC_CSN1            (0x880/sizeof(uint32_t *))
#define AM335x_CONF_GPMC_CSN2            (0x884/sizeof(uint32_t *))
#define AM335x_CONF_GPMC_CSN3            (0x888/sizeof(uint32_t *))
#define AM335x_CONF_GPMC_CLK             (0x88C/sizeof(uint32_t *))
#define AM335x_CONF_GPMC_ADVN_ALE        (0x890/sizeof(uint32_t *))
#define AM335x_CONF_GPMC_OEN_REN         (0x894/sizeof(uint32_t *))
#define AM335x_CONF_GPMC_WEN             (0x898/sizeof(uint32_t *))
#define AM335x_CONF_GPMC_BEN0_CLE        (0x89C/sizeof(uint32_t *))
#define AM335x_CONF_LCD_DATA0            (0x8A0/sizeof(uint32_t *))
#define AM335x_CONF_LCD_DATA1            (0x8A4/sizeof(uint32_t *))
#define AM335x_CONF_LCD_DATA2            (0x8A8/sizeof(uint32_t *))
#define AM335x_CONF_LCD_DATA3            (0x8AC/sizeof(uint32_t *))
#define AM335x_CONF_LCD_DATA4            (0x8B0/sizeof(uint32_t *))
#define AM335x_CONF_LCD_DATA5            (0x8B4/sizeof(uint32_t *))
#define AM335x_CONF_LCD_DATA6            (0x8B8/sizeof(uint32_t *))
#define AM335x_CONF_LCD_DATA7            (0x8BC/sizeof(uint32_t *))
#define AM335x_CONF_LCD_DATA8            (0x8C0/sizeof(uint32_t *))
#define AM335x_CONF_LCD_DATA9            (0x8C4/sizeof(uint32_t *))
#define AM335x_CONF_LCD_DATA10           (0x8C8/sizeof(uint32_t *))
#define AM335x_CONF_LCD_DATA11           (0x8CC/sizeof(uint32_t *))
#define AM335x_CONF_LCD_DATA12           (0x8D0/sizeof(uint32_t *))
#define AM335x_CONF_LCD_DATA13           (0x8D4/sizeof(uint32_t *))
#define AM335x_CONF_LCD_DATA14           (0x8D8/sizeof(uint32_t *))
#define AM335x_CONF_LCD_DATA15           (0x8DC/sizeof(uint32_t *))
#define AM335x_CONF_LCD_VSYNC            (0x8E0/sizeof(uint32_t *))
#define AM335x_CONF_LCD_HSYNC            (0x8E4/sizeof(uint32_t *))
#define AM335x_CONF_LCD_PCLK             (0x8E8/sizeof(uint32_t *))
#define AM335x_CONF_LCD_AC_BIAS_EN       (0x8EC/sizeof(uint32_t *))
#define AM335x_CONF_MMC0_DAT3            (0x8F0/sizeof(uint32_t *))
#define AM335x_CONF_MMC0_DAT2            (0x8F4/sizeof(uint32_t *))
#define AM335x_CONF_MMC0_DAT1            (0x8F8/sizeof(uint32_t *))
#define AM335x_CONF_MMC0_DAT0            (0x8FC/sizeof(uint32_t *))
#define AM335x_CONF_MMC0_CLK             (0x900/sizeof(uint32_t *))
#define AM335x_CONF_MMC0_CMD             (0x904/sizeof(uint32_t *))
#define AM335x_CONF_MII1_COL             (0x908/sizeof(uint32_t *))
#define AM335x_CONF_MII1_CRS             (0x90C/sizeof(uint32_t *))
#define AM335x_CONF_MII1_RX_ER           (0x910/sizeof(uint32_t *))
#define AM335x_CONF_MII1_TX_EN           (0x914/sizeof(uint32_t *))
#define AM335x_CONF_MII1_RX_DV           (0x918/sizeof(uint32_t *))
#define AM335x_CONF_MII1_TXD3            (0x91C/sizeof(uint32_t *))
#define AM335x_CONF_MII1_TXD2            (0x920/sizeof(uint32_t *))
#define AM335x_CONF_MII1_TXD1            (0x924/sizeof(uint32_t *))
#define AM335x_CONF_MII1_TXD0            (0x928/sizeof(uint32_t *))
#define AM335x_CONF_MII1_TX_CLK          (0x92C/sizeof(uint32_t *))
#define AM335x_CONF_MII1_RX_CLK          (0x930/sizeof(uint32_t *))
#define AM335x_CONF_MII1_RXD3            (0x934/sizeof(uint32_t *))
#define AM335x_CONF_MII1_RXD2            (0x938/sizeof(uint32_t *))
#define AM335x_CONF_MII1_RXD1            (0x93C/sizeof(uint32_t *))
#define AM335x_CONF_MII1_RXD0            (0x940/sizeof(uint32_t *))
#define AM335x_CONF_RMII1_REF_CLK        (0x944/sizeof(uint32_t *))
#define AM335x_CONF_MDIO                 (0x948/sizeof(uint32_t *))
#define AM335x_CONF_MDC                  (0x94C/sizeof(uint32_t *))
#define AM335x_CONF_SPI0_SCLK            (0x950/sizeof(uint32_t *))
#define AM335x_CONF_SPI0_D0              (0x954/sizeof(uint32_t *))
#define AM335x_CONF_SPI0_D1              (0x958/sizeof(uint32_t *))
#define AM335x_CONF_SPI0_CS0             (0x95C/sizeof(uint32_t *))
#define AM335x_CONF_SPI0_CS1             (0x960/sizeof(uint32_t *))
#define AM335x_CONF_ECAP0_IN_PWM0_OUT    (0x964/sizeof(uint32_t *))
#define AM335x_CONF_UART0_CTSN           (0x968/sizeof(uint32_t *))
#define AM335x_CONF_UART0_RTSN           (0x96C/sizeof(uint32_t *))
#define AM335x_CONF_UART0_RXD            (0x970/sizeof(uint32_t *))
#define AM335x_CONF_UART0_TXD            (0x974/sizeof(uint32_t *))
#define AM335x_CONF_UART1_CTSN           (0x978/sizeof(uint32_t *))
#define AM335x_CONF_UART1_RTSN           (0x97C/sizeof(uint32_t *))
#define AM335x_CONF_UART1_RXD            (0x980/sizeof(uint32_t *))
#define AM335x_CONF_UART1_TXD            (0x984/sizeof(uint32_t *))
#define AM335x_CONF_I2C0_SDA             (0x988/sizeof(uint32_t *))
#define AM335x_CONF_I2C0_SCL             (0x98C/sizeof(uint32_t *))
#define AM335x_CONF_MCASP0_ACLKX         (0x990/sizeof(uint32_t *))
#define AM335x_CONF_MCASP0_FSX           (0x994/sizeof(uint32_t *))
#define AM335x_CONF_MCASP0_AXR0          (0x998/sizeof(uint32_t *))
#define AM335x_CONF_MCASP0_AHCLKR        (0x99C/sizeof(uint32_t *))
#define AM335x_CONF_MCASP0_ACLKR         (0x9A0/sizeof(uint32_t *))
#define AM335x_CONF_MCASP0_FSR           (0x9A4/sizeof(uint32_t *))
#define AM335x_CONF_MCASP0_AXR1          (0x9A8/sizeof(uint32_t *))
#define AM335x_CONF_MCASP0_AHCLKX        (0x9AC/sizeof(uint32_t *))
#define AM335x_CONF_XDMA_EVENT_INTR0     (0x9B0/sizeof(uint32_t *))
#define AM335x_CONF_XDMA_EVENT_INTR1     (0x9B4/sizeof(uint32_t *))
#define AM335x_CONF_WARMRSTN             (0x9B8/sizeof(uint32_t *))
#define AM335x_CONF_NNMI                 (0x9C0/sizeof(uint32_t *))
#define AM335x_CONF_TMS                  (0x9D0/sizeof(uint32_t *))
#define AM335x_CONF_TDI                  (0x9D4/sizeof(uint32_t *))
#define AM335x_CONF_TDO                  (0x9D8/sizeof(uint32_t *))
#define AM335x_CONF_TCK                  (0x9DC/sizeof(uint32_t *))
#define AM335x_CONF_TRSTN                (0x9E0/sizeof(uint32_t *))
#define AM335x_CONF_EMU0                 (0x9E4/sizeof(uint32_t *))
#define AM335x_CONF_EMU1                 (0x9E8/sizeof(uint32_t *))
#define AM335x_CONF_RTC_PWRONRSTN        (0x9F8/sizeof(uint32_t *))
#define AM335x_CONF_PMIC_POWER_EN        (0x9FC/sizeof(uint32_t *))
#define AM335x_CONF_EXT_WAKEUP           (0xA00/sizeof(uint32_t *))
#define AM335x_CONF_USB0_DRVVBUS         (0xA1C/sizeof(uint32_t *))
#define AM335x_CONF_USB1_DRVVBUS         (0xA34/sizeof(uint32_t *))
#define AM335x_CQDETECT_STATUS           (0xE00/sizeof(uint32_t *))
#define AM335x_DDR_IO_CTRL               (0xE04/sizeof(uint32_t *))
#define AM335x_VTP_CTRL                  (0xE0C/sizeof(uint32_t *))
#define AM335x_VREF_CTRL                 (0xE14/sizeof(uint32_t *))
#define AM335x_TPCC_EVT_MUX_0_3          (0xF90/sizeof(uint32_t *))
#define AM335x_TPCC_EVT_MUX_4_7          (0xF94/sizeof(uint32_t *))
#define AM335x_TPCC_EVT_MUX_8_11         (0xF98/sizeof(uint32_t *))
#define AM335x_TPCC_EVT_MUX_12_15        (0xF9C/sizeof(uint32_t *))
#define AM335x_TPCC_EVT_MUX_16_19        (0xFA0/sizeof(uint32_t *))
#define AM335x_TPCC_EVT_MUX_20_23        (0xFA4/sizeof(uint32_t *))
#define AM335x_TPCC_EVT_MUX_24_27        (0xFA8/sizeof(uint32_t *))
#define AM335x_TPCC_EVT_MUX_28_31        (0xFAC/sizeof(uint32_t *))
#define AM335x_TPCC_EVT_MUX_32_35        (0xFB0/sizeof(uint32_t *))
#define AM335x_TPCC_EVT_MUX_36_39        (0xFB4/sizeof(uint32_t *))
#define AM335x_TPCC_EVT_MUX_40_43        (0xFB8/sizeof(uint32_t *))
#define AM335x_TPCC_EVT_MUX_44_47        (0xFBC/sizeof(uint32_t *))
#define AM335x_TPCC_EVT_MUX_48_51        (0xFC0/sizeof(uint32_t *))
#define AM335x_TPCC_EVT_MUX_52_55        (0xFC4/sizeof(uint32_t *))
#define AM335x_TPCC_EVT_MUX_56_59        (0xFC8/sizeof(uint32_t *))
#define AM335x_TPCC_EVT_MUX_60_63        (0xFCC/sizeof(uint32_t *))
#define AM335x_TIMER_EVT_CAPT            (0xFD0/sizeof(uint32_t *))
#define AM335x_ECAP_EVT_CAPT             (0xFD4/sizeof(uint32_t *))
#define AM335x_ADC_EVT_CAPT              (0xFD8/sizeof(uint32_t *))
#define AM335x_RESET_ISO                 (0x1000/sizeof(uint32_t *))
#define AM335x_DPLL_PWR_SW_CTRL          (0x1318/sizeof(uint32_t *))
#define AM335x_DDR_CKE_CTRL              (0x131C/sizeof(uint32_t *))
#define AM335x_SMA2                      (0x1320/sizeof(uint32_t *))
#define AM335x_M3_TXEV_EOI               (0x1324/sizeof(uint32_t *))
#define AM335x_IPC_MSG_REG0              (0x1328/sizeof(uint32_t *))
#define AM335x_IPC_MSG_REG1              (0x132C/sizeof(uint32_t *))
#define AM335x_IPC_MSG_REG2              (0x1330/sizeof(uint32_t *))
#define AM335x_IPC_MSG_REG3              (0x1334/sizeof(uint32_t *))
#define AM335x_IPC_MSG_REG4              (0x1338/sizeof(uint32_t *))
#define AM335x_IPC_MSG_REG5              (0x133C/sizeof(uint32_t *))
#define AM335x_IPC_MSG_REG6              (0x1340/sizeof(uint32_t *))
#define AM335x_IPC_MSG_REG7              (0x1344/sizeof(uint32_t *))
#define AM335x_DDR_CMD0_IOCTRL           (0x1404/sizeof(uint32_t *))
#define AM335x_DDR_CMD1_IOCTRL           (0x1408/sizeof(uint32_t *))
#define AM335x_DDR_CMD2_IOCTRL           (0x140C/sizeof(uint32_t *))
#define AM335x_DDR_DATA0_IOCTRL          (0x1440/sizeof(uint32_t *))
#define AM335x_DDR_DATA1_IOCTRL          (0x1444/sizeof(uint32_t *))
