#include <stdint.h>
#include <stdio.h>
#include <unistd.h>
#include <bbb_gpio.h>

int main(){
    // convert module & bit number to gpio number
    uint8_t gpio = MOD_BIT2GPIO(0,30);
    // initialize library (this is not necessary)
    bbb_gpio_init();
    // set direction to input
    if(bbb_gpio_direction_set(gpio, GPIO_INPUT)){
        fprintf(stderr, "Can not set GPIO1_24 to INPUT\n");
        return(1);
    }
    while(1){
        printf("disable\n");
        bbb_gpio_pud_en(gpio, PINMUX_PUD_DISABLE);
        printf("pulldown\n");
        bbb_gpio_pullup(gpio, PINMUX_PULLDOWN);
        printf("enable\n");
        bbb_gpio_pud_en(gpio, PINMUX_PUD_ENABLE);
        sleep(2);
        printf("disable\n");
        bbb_gpio_pud_en(gpio, PINMUX_PUD_DISABLE);
        printf("pullup\n");
        bbb_gpio_pullup(gpio, PINMUX_PULLUP);
        printf("enable\n");
        bbb_gpio_pud_en(gpio, PINMUX_PUD_ENABLE);
        sleep(2);
    }
    bbb_gpio_free();
    return(0);
}
