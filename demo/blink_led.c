#include <stdint.h>
#include <stdio.h>
#include <unistd.h>
#include <bbb_gpio.h>

/*
 * This demo blinks USER LED3 on BBB.
 * You need to run following command disabling control from LED driver.
 * echo none > /sys/class/leds/beaglebone:green:usr3/trigger
 */

int main(){
    // convert module & bit number to gpio number
    uint8_t gpio = MOD_BIT2GPIO(1,24);
    // initialize library (this is not necessary)
    bbb_gpio_init();
    // set direction to output
    if(bbb_gpio_direction_set(gpio, GPIO_OUTPUT)){
        fprintf(stderr, "Can not set GPIO1_24 to OUTPUT\n");
        return(1);
    }
    while(1){
        printf("clear\n");
        bbb_gpio_wr(gpio, 0);
        sleep(1);
        printf("set\n");
        bbb_gpio_wr(gpio, 1);
        sleep(1);
    }
    bbb_gpio_free();
    return(0);
}
