#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdint.h>
#include <sys/mman.h>
#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <bbb_gpio.h>
#include <am335x.h>
#include <syslog.h>
#include <string.h>

/* 
 * Getting GPIO module number and GPIO_OE bit from GPIO number
 * gpio[7]   - not used
 * gpio[6-5] - GPIO module number
 * gpio[4-0] - bit of GPIO_OE register
 */
#define GPIO2MOD(X)     ((X & 0x7f) >> 5)
#define GPIO2BIT(X)     (X & 0x1f)

uint16_t gpio_to_pinmux_off[] = {
    AM335x_NC,                     // gpio0[0]
    AM335x_NC,                     // gpio0[1]
    AM335x_CONF_SPI0_SCLK,         // gpio0[2]
    AM335x_CONF_SPI0_D0,           // gpio0[3]
    AM335x_CONF_SPI0_D1,           // gpio0[4]
    AM335x_CONF_SPI0_CS0,          // gpio0[5]
    AM335x_NC,                     // gpio0[6]
    AM335x_CONF_ECAP0_IN_PWM0_OUT, // gpio0[7]
    AM335x_CONF_LCD_DATA12,        // gpio0[8]
    AM335x_CONF_LCD_DATA13,        // gpio0[9]
    AM335x_CONF_LCD_DATA14,        // gpio0[10]
    AM335x_CONF_LCD_DATA15,        // gpio0[11]
    AM335x_CONF_UART1_CTSN,        // gpio0[12]
    AM335x_CONF_UART1_RTSN,        // gpio0[13]
    AM335x_CONF_UART1_RXD,         // gpio0[14]
    AM335x_CONF_UART1_TXD,         // gpio0[15]
    AM335x_NC,                     // gpio0[16]
    AM335x_NC,                     // gpio0[17]
    AM335x_NC,                     // gpio0[18]
    AM335x_NC,                     // gpio0[19]
    AM335x_CONF_XDMA_EVENT_INTR1,  // gpio0[20]
    AM335x_NC,                     // gpio0[21]
    AM335x_CONF_GPMC_AD8,          // gpio0[22]
    AM335x_CONF_GPMC_AD9,          // gpio0[23]
    AM335x_NC,                     // gpio0[24]
    AM335x_NC,                     // gpio0[25]
    AM335x_CONF_GPMC_AD10,         // gpio0[26]
    AM335x_CONF_GPMC_AD11,         // gpio0[27]
    AM335x_NC,                     // gpio0[28]
    AM335x_NC,                     // gpio0[29]
    AM335x_CONF_GPMC_WAIT0,        // gpio0[30]
    AM335x_CONF_GPMC_WPN,          // gpio0[31]
    AM335x_CONF_GPMC_AD0,          // gpio1[0]
    AM335x_CONF_GPMC_AD1,          // gpio1[1]
    AM335x_CONF_GPMC_AD2,          // gpio1[2]
    AM335x_CONF_GPMC_AD3,          // gpio1[3]
    AM335x_CONF_GPMC_AD4,          // gpio1[4]
    AM335x_CONF_GPMC_AD5,          // gpio1[5]
    AM335x_CONF_GPMC_AD6,          // gpio1[6]
    AM335x_CONF_GPMC_AD7,          // gpio1[7]
    AM335x_NC,                     // gpio1[8]
    AM335x_NC,                     // gpio1[9]
    AM335x_NC,                     // gpio1[10]
    AM335x_NC,                     // gpio1[11]
    AM335x_CONF_GPMC_AD12,         // gpio1[12]
    AM335x_CONF_GPMC_AD13,         // gpio1[13]
    AM335x_CONF_GPMC_AD14,         // gpio1[14]
    AM335x_CONF_GPMC_AD15,         // gpio1[15]
    AM335x_CONF_GPMC_A0,           // gpio1[16]
    AM335x_CONF_GPMC_A1,           // gpio1[17]
    AM335x_CONF_GPMC_A2,           // gpio1[18]
    AM335x_CONF_GPMC_A3,           // gpio1[19]
    AM335x_NC,                     // gpio1[20]
    AM335x_NC,                     // gpio1[21]
    AM335x_NC,                     // gpio1[22]
    AM335x_NC,                     // gpio1[23]
    AM335x_NC,                     // gpio1[24]
    AM335x_NC,                     // gpio1[25]
    AM335x_NC,                     // gpio1[26]
    AM335x_NC,                     // gpio1[27]
    AM335x_CONF_GPMC_BEN1,         // gpio1[28]
    AM335x_CONF_GPMC_CSN0,         // gpio1[29]
    AM335x_CONF_GPMC_CSN1,         // gpio1[30]
    AM335x_CONF_GPMC_CSN2,         // gpio1[31]
    AM335x_NC,                     // gpio2[0]
    AM335x_CONF_GPMC_CLK,          // gpio2[1]
    AM335x_CONF_GPMC_ADVN_ALE,     // gpio2[2]
    AM335x_CONF_GPMC_OEN_REN,      // gpio2[3]
    AM335x_CONF_GPMC_WEN,          // gpio2[4]
    AM335x_CONF_GPMC_BEN0_CLE,     // gpio2[5]
    AM335x_CONF_LCD_DATA0,         // gpio2[6]
    AM335x_CONF_LCD_DATA1,         // gpio2[7]
    AM335x_CONF_LCD_DATA2,         // gpio2[8]
    AM335x_CONF_LCD_DATA3,         // gpio2[9]
    AM335x_CONF_LCD_DATA4,         // gpio2[10]
    AM335x_CONF_LCD_DATA5,         // gpio2[11]
    AM335x_CONF_LCD_DATA6,         // gpio2[12]
    AM335x_CONF_LCD_DATA7,         // gpio2[13]
    AM335x_CONF_LCD_DATA8,         // gpio2[14]
    AM335x_CONF_LCD_DATA9,         // gpio2[15]
    AM335x_CONF_LCD_DATA10,        // gpio2[16]
    AM335x_CONF_LCD_DATA11,        // gpio2[17]
    AM335x_NC,                     // gpio2[18]
    AM335x_NC,                     // gpio2[19]
    AM335x_NC,                     // gpio2[20]
    AM335x_NC,                     // gpio2[21]
    AM335x_CONF_LCD_VSYNC,         // gpio2[22]
    AM335x_CONF_LCD_HSYNC,         // gpio2[23]
    AM335x_CONF_LCD_PCLK,          // gpio2[24]
    AM335x_CONF_LCD_AC_BIAS_EN,    // gpio2[25]
    AM335x_NC,                     // gpio2[26]
    AM335x_NC,                     // gpio2[27]
    AM335x_NC,                     // gpio2[28]
    AM335x_NC,                     // gpio2[29]
    AM335x_NC,                     // gpio2[30]
    AM335x_NC,                     // gpio2[31]
    AM335x_NC,                     // gpio3[0]
    AM335x_NC,                     // gpio3[1]
    AM335x_NC,                     // gpio3[2]
    AM335x_NC,                     // gpio3[3]
    AM335x_NC,                     // gpio3[4]
    AM335x_NC,                     // gpio3[5]
    AM335x_NC,                     // gpio3[6]
    AM335x_NC,                     // gpio3[7]
    AM335x_NC,                     // gpio3[8]
    AM335x_NC,                     // gpio3[9]
    AM335x_NC,                     // gpio3[10]
    AM335x_NC,                     // gpio3[11]
    AM335x_NC,                     // gpio3[12]
    AM335x_NC,                     // gpio3[13]
    AM335x_CONF_MCASP0_ACLKX,      // gpio3[14]
    AM335x_CONF_MCASP0_FSX,        // gpio3[15]
    AM335x_CONF_MCASP0_AXR0,       // gpio3[16]
    AM335x_CONF_MCASP0_AHCLKR,     // gpio3[17]
    AM335x_CONF_ECAP0_IN_PWM0_OUT, // gpio3[18]
    AM335x_CONF_MCASP0_FSR,        // gpio3[19]
    AM335x_CONF_XDMA_EVENT_INTR1,  // gpio3[20]
    AM335x_CONF_MCASP0_AHCLKX,     // gpio3[21]
    AM335x_NC,                     // gpio3[22]
    AM335x_NC,                     // gpio3[23]
    AM335x_NC,                     // gpio3[24]
    AM335x_NC,                     // gpio3[25]
    AM335x_NC,                     // gpio3[26]
    AM335x_NC,                     // gpio3[27]
    AM335x_NC,                     // gpio3[28]
    AM335x_NC,                     // gpio3[29]
    AM335x_NC,                     // gpio3[30]
    AM335x_NC                      // gpio3[31]
};

const uint32_t *Base_Addrs[] = {AM335x_GPIO0_BASE, AM335x_GPIO1_BASE, AM335x_GPIO2_BASE, AM335x_GPIO3_BASE};
 uint_fast32_t *GPIO_Addrs[4]={};
 uint_fast32_t *CM_PER_Addr=0;
 uint_fast32_t *CM_Addr=0;    // The library initialized if CM_Addr is non zero.

/* check whether library is initialized */
static inline void CHK_LIB_INIT(void){
#ifndef FAST
    if(!CM_Addr){
        if(bbb_gpio_init())
            exit(1);
    }
#endif
}

/* check maximum module number */
static inline void CHK_MOD_MAX(uint8_t x){
    if(x >= 4){
        errno=EINVAL;
        exit(1);
    }
}

bool bbb_gpio_init(void){
    int dev_map;
    if((dev_map = open("/dev/mem", O_RDWR|O_SYNC)) < 0){
        syslog(LOG_ERR, "Can not open /dev/mem. %s.\n", strerror(errno));
        return(1);
    }
    
    for (uint_fast8_t i=0; i<4; i++){
        if((GPIO_Addrs[i] = (uint_fast32_t *)mmap(0, AM335x_GPIO_LEN, PROT_READ | PROT_WRITE, MAP_SHARED, dev_map, (off_t)Base_Addrs[i])) == MAP_FAILED){
            syslog(LOG_ERR, "%s: Can not map GPIO register %p\n",__func__, Base_Addrs[i]);
            return(1);
        }
#ifdef DBG
        syslog(LOG_DEBUG, "%s: map %p to %p\n", __func__, Base_Addrs[i], GPIO_Addrs[i]);
#endif
    }
    if((CM_Addr = (uint_fast32_t *)mmap(0, AM335x_CM_LEN, PROT_READ | PROT_WRITE, MAP_SHARED, dev_map, (off_t)AM335x_CM_BASE)) == MAP_FAILED){
        syslog(LOG_ERR, "%s: Can not map CM register %p\n",__func__, AM335x_CM_BASE);
        return(1);
    }
#ifdef DBG
        syslog(LOG_DEBUG, "%s: map %p to %p\n", __func__, AM335x_CM_BASE, CM_Addr);
#endif
    if((CM_PER_Addr = (uint_fast32_t *)mmap(0, AM335x_CM_PER_LEN, PROT_READ | PROT_WRITE, MAP_SHARED, dev_map, (off_t)AM335x_CM_PER_BASE)) == MAP_FAILED){
        syslog(LOG_ERR, "%s: Can not map CM_PER register %p\n",__func__, AM335x_CM_PER_BASE);
        return(1);
    }
#ifdef DBG
        syslog(LOG_DEBUG, "%s: map %p to %p\n", __func__, AM335x_CM_PER_BASE, CM_PER_Addr);
#endif
    close(dev_map);
    return(0);
}

void bbb_gpio_free(void){
    if(CM_Addr){
        for (uint_fast8_t i=0; i<4; i++){
            munmap(GPIO_Addrs[i], AM335x_GPIO_LEN);
        }
        munmap(CM_Addr, AM335x_CM_LEN);
        CM_Addr = NULL;
    }
}

bool bbb_gpio_ctl(uint8_t num, enum bbb_gate_ratio ratio){
    CHK_LIB_INIT();
    CHK_MOD_MAX(num);
    if(ratio >= 7){
        errno = EINVAL;
        return(1);
    }
    uint_fast32_t *ptr = GPIO_Addrs[num]+GPIO_CTRL_OFF;
    *ptr = (uint32_t)ratio;
    if(*ptr != (uint32_t)ratio){
        syslog(LOG_ERR, "%s: Can not set GPIO_CTRL register at %p\n",__func__, ptr);
        return(1);
    }
    return(0);
}

bool bbb_gpio_config(uint8_t num, struct bbb_gpio_conf *cfg){
    CHK_LIB_INIT();
    if (bbb_gpio_direction_set(num, cfg->direction)) return (1);
    if (cfg->direction == GPIO_OUTPUT){
        if(bbb_gpio_wr(num, cfg->value)) return (1);
    }
    if (bbb_gpio_pud_en(num, cfg->pull_pud_en)) return (1);
    if (bbb_gpio_pullup(num, cfg->pull_upd)) return (1);
    return (0);
}

void bbb_gpio_mod_enable(uint8_t num){
    // enable GPIO clock if it is disabled
    // num - GPIO module number
    const size_t GPIO_CLKCTRL_OFF[] = {0, GPIO1_CLKCTRL_OFF, GPIO2_CLKCTRL_OFF, GPIO3_CLKCTRL_OFF};
    if(num == 0)    // CM_PER_GPIO0_CLKCTRL is not exist, the clock must be enabled always
        return;
    uint_fast32_t *ptr = CM_PER_Addr+GPIO_CLKCTRL_OFF[num];
    if( (*ptr & GPIO_CLKCTRL_MODULE_EN) == 0){
        syslog(LOG_DEBUG, "%s: GPIO%hhu module clock is enabled\n", __func__, num);
        *ptr |= GPIO_CLKCTRL_MODULE_EN;
    }
}

bool bbb_gpio_direction(uint8_t num, uint32_t oe){
    // oe[31-0] - 0=output, 1=input
    CHK_LIB_INIT();
    CHK_MOD_MAX(num);
    bbb_gpio_mod_enable(num);
    // set direction
    uint_fast32_t *ptr = GPIO_Addrs[num]+GPIO_OE_OFF;
#ifdef DBG
    syslog(LOG_DEBUG, "%s: @%p 0x%04x -> 0x%04x\n", __func__, ptr, *ptr, oe);
#endif
    *ptr = oe;
    if(*ptr != oe){
        syslog(LOG_ERR, "%s: Can not set GPIO_OE register at %p\n",__func__, ptr);
        return(1);
    }
    return(0);
}

bool bbb_gpio_direction_set(uint8_t gpio, bool val){
    // val - 0=output, 1=input
    CHK_LIB_INIT();
    bbb_gpio_mod_enable(GPIO2MOD(gpio));
    uint_fast32_t *ptr = GPIO_Addrs[GPIO2MOD(gpio)]+GPIO_OE_OFF;
    uint_fast32_t oe = (*ptr & ~(1<<GPIO2BIT(gpio))) | (val<<GPIO2BIT(gpio));

    return(bbb_gpio_direction(GPIO2MOD(gpio), oe));
}

bool bbb_gpio_direction_get(uint8_t gpio){
    CHK_LIB_INIT();
    bbb_gpio_mod_enable(GPIO2MOD(gpio));
    uint_fast32_t *ptr = GPIO_Addrs[GPIO2MOD(gpio)]+GPIO_OE_OFF;
    return((bool)(*ptr & (1<<GPIO2BIT(gpio))));
}

bool bbb_gpio_pinmux_set(uint8_t gpio, uint32_t pinmux){
    // pinmux value is described on
    // AM335x and AMIC110 Sitara Processors Technical Reference Manual,
    // 9.3.1.50 conf_<module>_<pin> Register
    CHK_LIB_INIT();
    uint_fast32_t *ptr = CM_Addr+gpio_to_pinmux_off[gpio];
#ifdef DBG
    syslog(LOG_DEBUG, "%s: @%p 0x%04x -> 0x%04x\n", __func__, ptr, *ptr, pinmux);
#endif
    *ptr = pinmux;
#ifdef DBG
    syslog(LOG_DEBUG, "Current 0x%04x\n", *ptr);
#endif
    if(*ptr != pinmux){
        syslog(LOG_ERR, "Err: %s: Can not set CM pinmux register at %p\n",__func__, ptr);
        return(1);
    }
    return(0);
}

uint32_t bbb_gpio_pinmux_get(uint8_t gpio){
    CHK_LIB_INIT();
    return(*(CM_Addr+gpio_to_pinmux_off[gpio]));
}

bool bbb_gpio_pud_en(uint8_t gpio, bool val){
    // val - 0=pullup/pulldown enabled, 1=pullup/pulldown disabled
    CHK_LIB_INIT();
    uint_fast32_t *ptr = CM_Addr+gpio_to_pinmux_off[gpio];
    uint_fast32_t pinmux = val ? (*ptr | PINMUX_PUDEN) : (*ptr & ~PINMUX_PUDEN);
    
    return(bbb_gpio_pinmux_set(gpio, pinmux));
}

bool bbb_gpio_pullup(uint8_t gpio, bool val){ 
    // val - 0=pulldown, 1= pulup
    CHK_LIB_INIT();
    uint_fast32_t *ptr = CM_Addr+gpio_to_pinmux_off[gpio];
    uint_fast32_t pinmux = val ? (*ptr | PINMUX_PUTYPE) : (*ptr & ~PINMUX_PUTYPE);

    return(bbb_gpio_pinmux_set(gpio, pinmux));
}

bool bbb_gpio_all_wr(uint8_t num, uint32_t val){
    CHK_MOD_MAX(num);
    CHK_LIB_INIT();
    uint_fast32_t *ptr = GPIO_Addrs[num]+GPIO_DATAOUT_OFF;
    *ptr = val;
    if( ((*ptr ^ val) & ~*(GPIO_Addrs[num]+GPIO_OE_OFF)) != 0){ // Is GPIO_DATAOUT changed by input?
        syslog(LOG_WARNING, "%s: Can not set GPIO value at %p\n",__func__, ptr);
        return(1);
    }
    return(0);
}

// direction should be set GPIO_DATAIN_OFF or GPIO_DATAOUT_OFF
uint32_t bbb_gpio_all_rd(uint8_t num, enum bbb_reg_off roff){
    CHK_MOD_MAX(num);
    CHK_LIB_INIT();
    return(*(GPIO_Addrs[num]+roff));
}

bool bbb_gpio_wr(uint8_t gpio, bool val){
    CHK_LIB_INIT();
    uint_fast32_t *ptr = GPIO_Addrs[GPIO2MOD(gpio)];
    ptr += val ? GPIO_SET_OFF : GPIO_CLEAR_OFF;
#ifdef DBG
    syslog(LOG_DEBUG, "%s: @%p 0x%04x ", __func__, ptr, *(GPIO_Addrs[GPIO2MOD(gpio)]+GPIO_DATAOUT_OFF));
#endif
    *ptr = (1<<GPIO2BIT(gpio));
#ifndef FAST
    if((bool)(*(GPIO_Addrs[GPIO2MOD(gpio)]+GPIO_DATAOUT_OFF) & (1<<GPIO2BIT(gpio))) != val){
        syslog(LOG_WARNING, "%s: Can not set GPIO%hhd-%hhd to %hhd\n",__func__, GPIO2MOD(gpio), GPIO2BIT(gpio), val);
        return(1);
    }
#endif
#ifdef DBG
    syslog(LOG_DEBUG, "-> 0x%04x\n", *(GPIO_Addrs[GPIO2MOD(gpio)]+GPIO_DATAOUT_OFF));
#endif
    return(0);
}

void bbb_gpio_set_hi(uint8_t gpio){
    CHK_LIB_INIT();
    uint_fast32_t *ptr = GPIO_Addrs[GPIO2MOD(gpio)] + GPIO_SET_OFF;
    *ptr = (1<<GPIO2BIT(gpio));
}

void bbb_gpio_set_low(uint8_t gpio){
    CHK_LIB_INIT();
    uint_fast32_t *ptr = GPIO_Addrs[GPIO2MOD(gpio)] + GPIO_CLEAR_OFF;
    *ptr = (1<<GPIO2BIT(gpio));
}

bool bbb_gpio_rd(uint8_t gpio){
    CHK_LIB_INIT();
    uint_fast32_t *ptr;
    if(bbb_gpio_direction_get(gpio))
        ptr = GPIO_Addrs[GPIO2MOD(gpio)]+GPIO_DATAIN_OFF;
    else
        ptr = GPIO_Addrs[GPIO2MOD(gpio)]+GPIO_DATAOUT_OFF;
    return((bool)(*ptr & (1<<GPIO2BIT(gpio))));
}
