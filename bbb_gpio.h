#pragma once
#include <stdint.h>
#include <stdbool.h>

#define MOD_BIT2GPIO(X, Y)  ((uint8_t)(X*32 + Y))

#define PINMUX_PULLDOWN 0
#define PINMUX_PULLUP   1
#define PINMUX_PUD_ENABLE   0
#define PINMUX_PUD_DISABLE  1

#define GPIO_OUTPUT 0
#define GPIO_INPUT  1
enum bbb_gate_ratio {
    DISABLE_MOD,
    GATING_FULL,
    GATING_DIV2,
    GATING_DIV4,
    GATING_DIV8
};

struct bbb_gpio_conf {
    uint32_t direction;     // 0=output, 1=input
    uint32_t value;
    uint32_t pull_pud_en;   // 0=enable, 1=disable
    uint32_t pull_upd;      // 0=pulldown, 1=pullup
};

enum bbb_reg_off { GPIO_DATAIN_OFF, GPIO_DATAOUT_OFF };

bool bbb_gpio_init(void);
void bbb_gpio_free(void);

bool bbb_gpio_config(uint8_t num, struct bbb_gpio_conf *cfg);
bool bbb_gpio_ctl(uint8_t num, enum bbb_gate_ratio ratio);
bool bbb_gpio_direction(uint8_t num, uint32_t oe);
bool bbb_gpio_direction_set(uint8_t gpio, bool val);
bool bbb_gpio_direction_get(uint8_t gpio);

bool bbb_gpio_all_wr(uint8_t num, uint32_t val);
uint32_t bbb_gpio_all_rd(uint8_t num, enum bbb_reg_off direction);
bool bbb_gpio_wr(uint8_t gpio, bool val);
void bbb_gpio_set_hi(uint8_t gpio);
void bbb_gpio_set_low(uint8_t gpio);
bool bbb_gpio_rd(uint8_t gpio);

// These function does not work because CM Module ragister is not writable from user space.
bool bbb_gpio_pmux_set(uint8_t gipi, uint32_t val);
uint32_t bbb_gpio_pinmux_get(uint8_t gpio);
bool bbb_gpio_pud_en(uint8_t gipi, bool val);
bool bbb_gpio_pullup(uint8_t gipi, bool val);

#define HI(x)   bbb_gpio_set_hi(x)
#define LOW(x)  bbb_gpio_set_low(x)

/*
static inline void HI(uint8_t gpio){
    bbb_gpio_set_hi(gpio);
}

static inline void LOW(uint8_t gpio){
    bbb_gpio_set_low(gpio);
}
*/
