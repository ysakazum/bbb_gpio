CC=gcc
#CFLAGS=-Wall -std=gnu99 -O2 -DDBG -I .
CFLAGS=-Wall -std=gnu99 -O3 -I .
CXXFLAGS=-std=c++2a -O3 -Wall -Wextra -I .

bbb_gpio.o: bbb_gpio.c
	$(CC) $(CFLAGS) -c $<

cpp: bbb_gpio.c
	$(CXX) $(CXXFLAGS) -c $<

clean:
	rm -f *.o
